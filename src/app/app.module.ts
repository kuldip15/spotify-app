import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import {HttpClientModule} from '@angular/common/http';
import {SpotifyAPIService} from './services/spotify.service';
import {LeftPanelComponent} from './components/leftpanel/leftpanel.component';
import {SafePipe} from './shared/pipe/Safe.pipe';
@NgModule({
  declarations: [
    AppComponent,
    LeftPanelComponent,
    SafePipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule  ,
    FormsModule      
    
  ],
  providers: [SpotifyAPIService],
  bootstrap: [AppComponent]
})
export class AppModule { }
