import { Injectable } from '@angular/core';
import { Observable,Subject,BehaviorSubject, throwError } from 'rxjs';
import { catchError, retry ,tap,map} from 'rxjs/operators';
import { HttpClient,HttpHeaders,HttpErrorResponse } from '@angular/common/http';
@Injectable()
export class SpotifyAPIService {
  client_id = "d78bf7a2307548aa9e0d18c28fbcbf1e";
  client_secret = "b6ca4d359eba499bbe9c7fbb0ffad22c";

  private accessToken: any;
  private tokenType: string;
  private $CurrentTrack: BehaviorSubject<string> = new BehaviorSubject("");
  private $Code: BehaviorSubject<string> = new BehaviorSubject("");
  private $userData: BehaviorSubject<string> = new BehaviorSubject("");
  constructor(private http: HttpClient) { }

  private extractData(res: Response) {
    let body = res;
    return body || { };
  }
  

  getcurrenttrack(): Observable<any> {
    return this.$CurrentTrack;
  }

  setcurrenttrack(id: any) {

    this.$CurrentTrack.next(id)
  }

  
  getcode(): Observable<any> {
    return this.$Code;
  }

  setCode(id: any) {

    this.$Code.next(id)
  }

  getRefreshtoken(){
    
    const options = this.getOptions1();
    return this.http.get(`https://spotify-service-15.herokuapp.com/cred`,options)
    .pipe(
      tap( // Log the result or error
        (data:any) => {
            this.accessToken = data.payload.access_token;
            this.tokenType = data.payload.token_type;
            
        },
        error => console.log(error)
      ));
  }

  getUserDetails() {
    //const options = this.getOptions();
     
    let header = new HttpHeaders()
    .set('Authorization', 'Bearer' + ' ' + this.accessToken)
    .set('Content-Type', 'application/json')
    .set('Access-Control-Allow-Origin', '*')
    .set('Access-Control-Allow-Headers', 'Content-Type')
    .set('Access-Control-Allow-Methods', 'GET,POST')
    let options = { headers: header }
    return this.http.get(`https://api.spotify.com/v1/me`, options)
    .pipe(
      map(this.extractData));
   
      
  }

  searchAlbums(title: string) {
    const options = this.getOptions();
    return this.http.get(`https://api.spotify.com/v1/search?query=${title}&type=album`, options)
    .pipe(
        map(this.extractData));
      
  }

  loadAlbum(id) {
    const options = this.getOptions();
    return this.http.get(`https://api.spotify.com/v1/albums/${id}`, options)
    .pipe(
        map(this.extractData));
  }

  loadNewReleases() {
    const options = this.getOptions();
    return this.http.get(`https://api.spotify.com/v1/browse/new-releases?country=SE&limit=10&offset=5`, options)
    .pipe(
        map(this.extractData));
  }

  private handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an observable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  };
  private getOptions1() {
    
    let header = new HttpHeaders().set('Access-Control-Allow-Origin', '*')
    .set('Access-Control-Allow-Headers', 'Origin, Accept, Authorization, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers')
    .set('Access-Control-Allow-Credentials', 'true')
    .set('Access-Control-Allow-Methods', 'GET, POST, DELETE, PUT, HEAD, OPTIONS')
    // .set('Content-Type', 'application/json')

    
    let options = { headers: header }

    return options;
  }

  private getOptions() {
   
    let header = new HttpHeaders()
    .set('Authorization', this.tokenType + ' ' + this.accessToken)
    .set('Content-Type', 'application/json')
    .set('Access-Control-Allow-Origin', '*')
    .set('Access-Control-Allow-Headers', 'Content-Type')
    .set('Access-Control-Allow-Methods', 'GET,POST')
     // header.set('Access-Control-Allow-Origin', '*');

    
    let options = { headers: header }

    return options;
  }
}