import {Component }from '@angular/core'; 
import {SpotifyAPIService }from './services/spotify.service'; 
import {Router,ActivatedRoute }from '@angular/router'; 
import {debounceTime }from 'rxjs/operators'; 
import {LeftPanelComponent}from './components/leftpanel/leftpanel.component'; 
import {DomSanitizer, SafeResourceUrl, SafeUrl }from '@angular/platform-browser'; 


declare var jQuery:any; 
const $ = jQuery; 
@Component( {
selector:'app-root', 
templateUrl:'./app.component.html', 
styleUrls:['./app.component.scss']
})
export class AppComponent {
title = 'spotify-app'; 
albums:any = []; 
tracks:any = []; 
searchquery:string = ""; 
searchResult:any = []; 
loadLeftPanel:string = ""; 
currenttrack:any; 

constructor(private sanitizer:DomSanitizer,private route: ActivatedRoute,
     private router:Router, public rest:SpotifyAPIService) {}


ngOnInit() {
   
this.rest.getRefreshtoken().subscribe(res =>  {
if (res) {
this.getProducts(); 
this.setCurrentTrack(); 
this.rest.getUserDetails().subscribe(res =>  {
    if(res){
        console.log("details" + res)
    }
});
}
}); 

}



search() {
if (this.searchquery && this.searchquery.length > 0) {
this.rest.searchAlbums(this.searchquery)
.pipe(debounceTime(500))
.subscribe((data: {}) =>  {
console.log(data); 
this.searchResult = this.maptoAlbumModel(data); 
}); }else{
    this.searchResult=[];
}
}

redirect() {
window.location.href = 'https://accounts.spotify.com/authorize' +
'?response_type=code' + '&scope=' + "user-read-private%20user-read-email" + 
'&client_id=d78bf7a2307548aa9e0d18c28fbcbf1e&redirect_uri=http://sponation.com/&show_dialog=true';
}
getProducts() {
this.albums = []; 
this.rest.loadNewReleases().subscribe((data: {}) =>  {
console.log(data); 
this.albums = this.maptoAlbumModel(data); 
}); 
}

onClickAlbum(event) {
this.rest.loadAlbum(event).subscribe((data: {}) =>  {
console.log(data); 
this.loadLeftPanel = "open"; 
this.tracks = this.maptoTrackModel(data); 
}); 
}
//src="https://embed.spotify.com/?uri="
setCurrentTrack() {
this.rest.getcurrenttrack().subscribe((data) =>  {
if (data) {

    this.currenttrack = "https://embed.spotify.com/?uri=" +data;
  

 $('#unique123').attr('src', this.currenttrack); 
}

}); 
}

maptoAlbumModel(data:any) {
let resultset = []; 
data.albums.items.map(item =>  {
let obj =  {
name:item.name, 
type:item.album_type, 
id:item.id, 
image:item.images[2].url, 
release:item.release_date
            }
resultset.push(obj); 
})
return resultset; 
}

maptoTrackModel(data:any) {
let resultset = {}; 
resultset['data']=[];
resultset['name']=data.name;
resultset['label']=data.label;
resultset['image']=data.images[2].url
data.tracks.items.map(item =>  {
let obj =  {
name:item.name, 
artistname:item.artists[0].name, 
type:item.album_type, 
id:item.id, 
uri:item.uri, 
preview:item.preview_url, 
track_number:item.track_number

           }
           resultset['data'].push(obj); 
})
return resultset; 
}
}
