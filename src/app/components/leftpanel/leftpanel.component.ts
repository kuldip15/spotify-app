import {Component,
    Input ,SimpleChanges }from '@angular/core'; 
    import {SpotifyAPIService }from '../../services/spotify.service'; 
@Component( {
selector:'left-panel', 
templateUrl:'./leftpanel.component.html'})
export class LeftPanelComponent {
    @Input() data=[];
    @Input() isShowLeftPanel ="";

    currentAlbum:any;
    constructor( public rest:SpotifyAPIService) {}

    ngOnChanges(changes: SimpleChanges) {
        // only run when property "data" changed
        if (changes['data']) {
            this.currentAlbum = this.data;
            if(this.currentAlbum && this.currentAlbum.data && this.currentAlbum.data.length >0){
                this.isShowLeftPanel="open";
            }
            
        }
    }

    addCurrentsongtoplay(track){
        this.rest.setcurrenttrack(track);
    }

    closed(){
        this.isShowLeftPanel="";
    }
}